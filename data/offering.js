export const jeans = [
    {
        name: 'Orlo a macchina',
        price: '5,00 €'
    },
    {
        name: 'Orlo punti invisibili',
        price: '10,00 €'
    },
    {
        name: 'Orlo punti invisibili con battitacco',
        price: '12,00 €'
    },
    {
        name: 'Orlo con risvolto',
        price: '10,00 €'
    },
    {
        name: 'Ordlo pantalone punto di copertura',
        price: '10,00 €'
    },
    {
        name: 'Sostituzionae lampo pantalone (solo manodopera)',
        price: '8,00 €'
    },
    {
        name: 'Sostituzione lampo jeans (solo manodopera)',
        price: '10,00 €'
    },
    {
        name: 'Allargare/stringere vita dietro cinturino (pantalone)',
        price: '7,00 €'
    },
    {
        name: 'Stringere vita dietro cinturino (jeans)',
        price: '15,00 €'
    },
    {
        name: 'Allargare/stringere gamba (pantalone)',
        price: '8,00 €'
    },
    {
        name: 'Stringere gamba (jeans)',
        price: '8,00 €'
    },
    {
        name: 'Applicazione toppa (solo manodopera)',
        price: '5,00 €'
    },
    {
        name: 'Fodera tasca (pantalone/jeans)',
        price: '10,00 €'
    },
]

export const coats = [
    {
        name: 'Accorciare manica foderata',
        price: '10,00 €'
    },
    {
        name: 'Accorciare manica foderata con apertura',
        price: '20,00 €'
    },
    {
        name: 'Stringere/allargare dietro foderato',
        price: '15,00 €'
    },
    {
        name: 'Stringere/allargare dietro sfoderato',
        price: '10,00 €'
    },
    {
        name: 'Stringere/allargare fianchi foderato',
        price: '15,00 €'
    },
    {
        name: 'Stringere/allargare fianchi sfoderato',
        price: '10,00 €'
    },
    {
        name: 'Stringere/allargare dietro foderato',
        price: '15,00 €'
    },
    {
        name: 'Orlo fondo dritto sfoderato cappotto',
        price: '15,00 €'
    },
    {
        name: 'Orlo fondo spacco foderato cappotto',
        price: '25,00 €'
    },
    {
        name: 'Orlo fondo spacco sfoderato cappotto',
        price: '15,00 €'
    },
    {
        name: 'Orlo fondo dritto foderato cappotto',
        price: '25,00 €'
    },
    {
        name: 'Cambio fodera tasche cappotto',
        price: '10,00 €'
    },
    {
        name: 'Cambio fodera manica cappotto (solo manodopera)',
        price: '20,00 €'
    },
    {
        name: 'Cambio fodera totale (solo manodopera)',
        price: '50,00 €'
    },
]

export const gowns = [
    {
        name: 'Orlo a macchina',
        price: '8,00 €'
    },
    {
        name: 'Orlo a mano',
        price: '10,00 €'
    },
    {
        name: 'Orlo foderato',
        price: '12,00 €'
    },
    {
        name: 'Orlo con spacchi',
        price: '15,00 €'
    },
    {
        name: 'Allargare/stringere fianchi/vita semplice',
        price: '10,00 €'
    },
    {
        name: 'Allargare/stringere fianchi/vita sfoderato',
        price: '15,00 €'
    },
    {
        name: 'Cambio fodera gonna',
        price: '10,00 €'
    },
    {
        name: 'Cambio cerniera (solo manodopera)',
        price: '7,00 €'
    },
    {
        name: 'Cambio cerniera invisibile (solo manodopera)',
        price: '10,00 €'
    },
]
