export default defineAppConfig({
    title: 'Annarù',
    ui: {
        button: {
            default: {
                color: 'silver'
            }
        },
        primary: 'stone',
        gray: 'cool'
    },
});
